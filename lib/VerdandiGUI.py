#!/usr/bin/env python3
#                                                            05-MAR-2017
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from textwrap import dedent

from lib.ReadOnlyText import ReadOnlyText
from lib.VerdandiApp import VerdandiApp
from lib.NewSQLiteDialog import NewSQLiteDialog
from lib.ConnectDBDialog import ConnectDBDialog


class VerdandiGUI(tk.Frame):
	"""Defines the Verdandi GUI"""
	def __init__(self, parent,suppliedOpts):
		self.__suppliedOpts = suppliedOpts
		self.parent = parent
		self.name = tk.StringVar()
		self.searchText = tk.StringVar()
		self.newRecordName = tk.StringVar()
		self.editRecordName = tk.StringVar()
		self.searchText.set('')
		self.currentRecordName = ''
		self.currentScreenName = 'main'
		self.app = ""
		self.screenWidth = self.parent.winfo_screenwidth()
		self.screenHeight = self.parent.winfo_screenheight()
		self.init_app()
		self.init_GUI()
		
	def init_app(self):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.init_app")
		self.app = VerdandiApp(self,self.__suppliedOpts)

	def init_GUI(self):
		self.backColor = self.app.theme['COLORS']['background']
		self.foreColor = self.app.theme['COLORS']['foreground']
		self.textBackColor = self.app.theme['COLORS']['textbackground']
		self.textForeColor = self.app.theme['COLORS']['textforeground']
		self.focusColor = self.app.theme['COLORS']['focus']
		self.noFocusColor = self.app.theme['COLORS']['background']
		# For Buttons
		self.buttonBGColor = self.app.theme['COLORS']['background']
		self.activeFGColor = self.app.theme['COLORS']['background']
		self.activeBGColor = self.app.theme['COLORS']['foreground']
		self.fontName = self.app.theme['FONT']['name']
		self.fontSize = self.app.theme['FONT']['size']
		self.fontStyle = self.app.theme['FONT']['style']
		self.font = (self.fontName, self.fontSize, self.fontStyle)

		# parent Frame ------------------------------------------------
		tk.Frame.__init__(self, self.parent, borderwidth=0, background=self.backColor,
							highlightbackground=self.noFocusColor)
		# These help make the window stretchy
		self.grid_columnconfigure(1, weight=1)
		self.grid_rowconfigure(2, weight=1)
				
		# Menu Button Frame -------------------------------
		menubar = tk.Menu(self.parent, borderwidth=0, font=self.font, background=self.backColor, foreground=self.foreColor,
							activebackground=self.activeBGColor, activeforeground=self.activeFGColor)
		self.parent.config(menu=menubar)
		newDatabaseMenu = tk.Menu(tearoff=0, font=self.font, background=self.backColor, foreground=self.foreColor,
									activebackground=self.activeBGColor, activeforeground=self.activeFGColor)
		newDatabaseMenu.add_command(label="SQLite3 Database", font=self.font, background=self.backColor, foreground=self.foreColor, command=self.new_sqlite3_database)
		fileMenu = tk.Menu(menubar, tearoff=0, font=self.font, background=self.backColor, foreground=self.foreColor,
								activebackground=self.activeBGColor, activeforeground=self.activeFGColor,)
		fileMenu.add_cascade(label="New", menu=newDatabaseMenu)
		fileMenu.add_command(label="Connect Database",
									command=self.connect_database_dialog)
		fileMenu.add_command(label="Change Passphrase",
									command=self.change_passphrase_button)
		fileMenu.add_separator()
		fileMenu.add_command(label="Exit",
									command=lambda: self.parent.destroy())
		editMenu = tk.Menu(menubar, tearoff=0, font=self.font, background=self.backColor, foreground=self.foreColor,
							activebackground=self.activeBGColor, activeforeground=self.activeFGColor,)
		editMenu.add_command(label='Cut',
									command=self.cut_text)
		editMenu.add_command(label='Copy',
									command=self.copy_text)
		editMenu.add_command(label='Paste',
									command=self.paste_text)
		editMenu.add_command(label='Select All',
									command=self.select_all_text)
		recordMenu = tk.Menu(menubar, tearoff=0, font=self.font, background=self.backColor, foreground=self.foreColor,
								activebackground=self.activeBGColor, activeforeground=self.activeFGColor,)
		recordMenu.add_command(label="Add New Record",
									command=self.draw_add_record_screen)
		recordMenu.add_command(label="Edit Current Record",
									command=self.draw_edit_record_screen)
		recordMenu.add_command(label="Delete Current Record",
									command=self.delete_record_request)
		menubar.add_cascade(label="File", menu=fileMenu)
		menubar.add_cascade(label="Edit", menu=editMenu)
		menubar.add_cascade(label="Record", menu=recordMenu)

		#
		#	New and Edit Record Frame
		#
		self.newAndEditRecordButtonFrame = tk.Frame(self, background=self.backColor, highlightbackground=self.noFocusColor,
											highlightcolor=self.backColor, highlightthickness=1)
		self.newAndEditRecordButtonFrame.grid(row=0, column=0, columnspan=2, sticky='w'+'e')
		self.newRecordNameLabel = tk.Label(self.newAndEditRecordButtonFrame, text="Name: ", font=self.font, 
										background=self.backColor, foreground=self.foreColor,
										highlightbackground=self.backColor,
										highlightcolor=self.backColor, highlightthickness=1)
		self.newRecordNameLabel.grid(row=0, column=0, sticky='w')
		self.editRecordNameLabel = tk.Label(self.newAndEditRecordButtonFrame, text="Name: ", font=self.font, 
											background=self.backColor, foreground=self.foreColor,
											highlightbackground=self.backColor,
											highlightcolor=self.backColor, highlightthickness=1)
		self.editRecordNameLabel.grid(row=0, column=0, sticky='w')
		self.newRecordNameEntry = tk.Entry(self.newAndEditRecordButtonFrame, font=self.font, 
										background=self.textBackColor, foreground=self.textForeColor,
										insertbackground=self.textForeColor,
										highlightbackground=self.noFocusColor,
										highlightcolor=self.focusColor, highlightthickness=1,
										textvariable=self.newRecordName)
		self.newRecordNameEntry.grid(row=0, column=1, sticky='w')
		self.editRecordNameEntry = tk.Entry(self.newAndEditRecordButtonFrame, font=self.font, 
										background=self.textBackColor, foreground=self.textForeColor,
										insertbackground=self.textForeColor,
										highlightbackground=self.noFocusColor,
										highlightcolor=self.focusColor, highlightthickness=1,
										textvariable=self.editRecordName)
		self.editRecordNameEntry.grid(row=0, column=1, sticky='w')
		self.addNewRecordButton = tk.Button(self.newAndEditRecordButtonFrame, borderwidth=0, font=self.font, 
										text="Commit New Record",
										background=self.buttonBGColor, foreground=self.foreColor,
										activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
										highlightbackground=self.noFocusColor,
										highlightcolor=self.focusColor, highlightthickness=1,
										command=self.add_record_commit)
		self.addNewRecordButton.grid(row=0, column=2, sticky='w')
		self.editExistingRecordButton = tk.Button(self.newAndEditRecordButtonFrame, borderwidth=0, font=self.font, 
										text="Commit Record Change",
										background=self.buttonBGColor, foreground=self.foreColor,
										activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
										highlightbackground=self.noFocusColor,
										highlightcolor=self.focusColor, highlightthickness=1,
										command=self.edit_record_commit)
		self.editExistingRecordButton.grid(row=0, column=2, sticky='w')
		self.cancelNewRecordButton = tk.Button(self.newAndEditRecordButtonFrame, borderwidth=0, font=self.font, 
										text="Cancel",
										background=self.buttonBGColor, foreground=self.foreColor,
										activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
										highlightbackground=self.noFocusColor,
										highlightcolor=self.focusColor, highlightthickness=1,
										command=self.leave_add_record_screen)
		self.cancelNewRecordButton.grid(row=0, column=3, sticky='e')
		self.cancelEditRecordButton = tk.Button(self.newAndEditRecordButtonFrame, borderwidth=0, font=self.font, 
										text="Cancel",
										background=self.buttonBGColor, foreground=self.foreColor,
										activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
										highlightbackground=self.noFocusColor,
										highlightcolor=self.focusColor, highlightthickness=1,
										command=self.leave_edit_record_screen)
		self.cancelEditRecordButton.grid(row=0, column=3, sticky='e')
		self.newRecordNameLabel.grid_remove()
		self.editRecordNameLabel.grid_remove()
		self.newRecordNameEntry.grid_remove()
		self.editRecordNameEntry.grid_remove()
		self.addNewRecordButton.grid_remove()
		self.editExistingRecordButton.grid_remove()
		self.cancelNewRecordButton.grid_remove()
		self.cancelEditRecordButton.grid_remove()
		self.newAndEditRecordButtonFrame.grid_remove()

		#
		#	Search Entry Frame
		#
		self.searchEntryFrame = tk.Frame(self, borderwidth=0, background=self.backColor, highlightbackground=self.noFocusColor,
											highlightcolor=self.noFocusColor, highlightthickness=1)
		self.searchEntryFrame.grid(row=1, column=0, sticky='n'+'s'+'e'+'w')
		self.searchEntry = tk.Entry(self.searchEntryFrame, font=self.font, 
										background=self.textBackColor, foreground=self.textForeColor,
										insertbackground=self.textForeColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,
										textvariable=self.searchText)
		self.searchEntry.bind("<Return>", lambda event: self.query_records())
		self.searchEntry.grid(row=0, column=0, sticky='n'+'s'+'e'+'w')
		self.searchButton = tk.Button(self.searchEntryFrame, borderwidth=0,
											text="%",
											font=self.font,
											#font=("Helvatica", 7, "bold"),
											background=self.buttonBGColor, foreground=self.foreColor,
											activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
											highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
											highlightthickness=1,
											command=self.query_records)
		self.searchButton.bind("<Return>", lambda event: self.query_records())
		self.searchButton.grid(row=0, column=1, sticky='e')

		#
		#	Read-Only Text Box Frame
		#
		self.readOnlyTextFrame = tk.Frame(self, borderwidth=0, background=self.backColor, highlightbackground=self.noFocusColor,
											highlightcolor=self.noFocusColor, highlightthickness=1)
		self.readOnlyTextFrame.grid(row=1, column=1, rowspan=2,
										sticky='n'+'s'+'e'+'w')
		self.readOnlyTextFrame.grid_rowconfigure(0, weight=1)
		self.readOnlyTextFrame.grid_columnconfigure(0, weight=1)
        #self.readOnlyText = tk.Text(self.readOnlyTextFrame, font=self.font, 
		self.readOnlyText = ReadOnlyText(self.readOnlyTextFrame, font=self.font, 
								width=72, height=25,
								background=self.textBackColor, foreground=self.textForeColor,
								insertbackground=self.textForeColor,
								highlightbackground=self.noFocusColor,highlightcolor=self.focusColor, highlightthickness=1,
								wrap='none',borderwidth=0, insertwidth=0)
		self.readOnlyText.bind("<Button-3>", self.readonly_text_popup)
		self.readOnlyText.grid(row=0, column=0, sticky=tk.N+tk.S+tk.E+tk.W)
		self.readOnlyTextScroll = tk.Scrollbar(self.readOnlyTextFrame,
										orient='vertical',
										background=self.backColor,
										activebackground=self.activeBGColor,
										highlightbackground=self.noFocusColor,highlightcolor=self.noFocusColor, highlightthickness=1,
										command=self.readOnlyText.yview)
		self.readOnlyTextScroll.grid(row=0, column=1, sticky=tk.N+tk.S)
		self.readOnlyText.configure(yscrollcommand=self.readOnlyTextScroll.set)
		self.readOnlyTextMenu = tk.Menu(self.readOnlyTextFrame, font=self.font, tearoff=0, background=self.backColor, foreground=self.foreColor,
									activebackground=self.activeBGColor, activeforeground=self.activeFGColor)
		self.readOnlyTextMenu.add_command(label="Copy", command=self.copy_text)
		self.readOnlyTextMenu.add_command(label="Select All", command=self.select_all_text)

		# Edit Text Box Frame -----------------------------------------------
		self.editTextFrame = tk.Frame(self, background=self.backColor,
										highlightbackground=self.noFocusColor,highlightcolor=self.noFocusColor, highlightthickness=1,)
		self.editTextFrame.grid(row=1, column=1, rowspan=2,
										sticky='n'+'s'+'e'+'w')
		self.editTextFrame.grid_rowconfigure(0, weight=1)
		self.editTextFrame.grid_columnconfigure(0, weight=1)
		self.editText = tk.Text(self.editTextFrame, width=72, height=25, font=self.font, 
								background=self.textBackColor, foreground=self.textForeColor,
								insertbackground=self.textForeColor,
								highlightbackground=self.noFocusColor,highlightcolor=self.focusColor, highlightthickness=1,
								wrap='none',borderwidth=0)
		self.editText.bind("<Button-3>", self.edit_text_popup)
		self.editText.grid(row=0, column=0, sticky='n'+'s'+'e'+'w')
		self.editTextScroll = tk.Scrollbar(self.editTextFrame, orient='vertical',
										background=self.backColor,
										activebackground=self.activeBGColor,
										highlightbackground=self.noFocusColor,
										highlightcolor=self.noFocusColor, highlightthickness=1,
										command=self.editText.yview)
		self.editTextScroll.grid(row=0, column=1, sticky=tk.N+tk.S)
		self.editText.configure(yscrollcommand=self.editTextScroll.set)
		self.editTextMenu = tk.Menu(self.editTextFrame, font=self.font, tearoff=0, background=self.backColor, foreground=self.foreColor,
									activebackground=self.activeBGColor, activeforeground=self.activeFGColor)
		self.editTextMenu.add_command(label="Cut", command=self.cut_text)
		self.editTextMenu.add_command(label="Copy", command=self.copy_text)
		self.editTextMenu.add_command(label="Paste", command=self.paste_text)
		self.editTextMenu.add_command(label="Select All", command=self.select_all_text)
		self.editTextMenu.add_command(label="Random String", command=self.get_random_string)
		self.editTextScroll.grid_remove()
		self.editText.grid_remove()
		self.editTextFrame.grid_remove()

		# Listbox Frame ------------------------------------------------
		self.listBoxFrame = tk.Frame(self, borderwidth=0, background=self.backColor,
									highlightbackground=self.noFocusColor,
									highlightcolor=self.noFocusColor, highlightthickness=1)
		self.listBoxFrame.grid(row=2, column=0, sticky='n'+'s'+'e'+'w')
		self.listBoxFrame.grid_rowconfigure(0, weight=1)
		self.listBoxFrame.grid_columnconfigure(0, weight=1)
		self.yRecordScroll = tk.Scrollbar(self.listBoxFrame, orient=tk.VERTICAL,
									background=self.backColor,
									activebackground=self.activeBGColor,
									highlightbackground=self.noFocusColor,
									highlightcolor=self.noFocusColor, highlightthickness=1)
		self.yRecordScroll.grid(row=0, column=1, sticky='n'+'s')
		self.recordList = tk.Listbox(self.listBoxFrame, selectmode='single', borderwidth=0, font=self.font, 
									background=self.textBackColor, foreground=self.textForeColor,
									highlightbackground=self.noFocusColor,
									highlightcolor=self.noFocusColor, highlightthickness=1,
									yscrollcommand=self.yRecordScroll.set)
		self.recordList.bind("<Double-Button-1>", self.execute_selection)
		self.recordList.grid(row=0, column=0, sticky='n'+'s'+'e'+'w')
		self.yRecordScroll['command'] = self.recordList.yview
		
		# With the GUI setup, get some data from app
		if self.app.check_passphrase(""):
			self.app.copy_db_to_memdb()
			# Populate the list Box
			self.query_records()
		else:
			self.password_dialog()

	# -----------------------------------------------------------------
	# Event Handlers
	# -----------------------------------------------------------------

	#
	#	Record Menu Handlers
	#
	def draw_add_record_screen(self):
		if self.currentScreenName == 'main':
			self.searchButton.grid_remove()
			self.searchEntry.grid_remove()
			self.searchEntryFrame.grid_remove()
			self.recordList.grid_remove()
			self.yRecordScroll.grid_remove()
			self.listBoxFrame.grid_remove()
			self.listBoxFrame.grid_remove()
			self.readOnlyTextScroll.grid_remove()
			self.readOnlyText.grid_remove()
			self.readOnlyTextFrame.grid_remove()
			self.newAndEditRecordButtonFrame.grid()
			self.newRecordNameLabel.grid()
			self.newRecordNameEntry.grid()
			self.addNewRecordButton.grid()
			self.cancelNewRecordButton.grid()
			self.editTextFrame.grid()
			self.editText.grid()
			self.editTextScroll.grid()
		else:
			self.leave_edit_record_screen()
			self.draw_add_record_screen()

	def leave_add_record_screen(self):
		self.newRecordName.set("")
		self.editText.delete('1.0','end-1c')
		self.newRecordNameLabel.grid_remove()
		self.newRecordNameEntry.grid_remove()
		self.addNewRecordButton.grid_remove()
		self.cancelNewRecordButton.grid_remove()
		self.newAndEditRecordButtonFrame.grid_remove()
		self.editTextScroll.grid_remove()
		self.editText.grid_remove()
		self.editTextFrame.grid_remove()
		self.searchEntryFrame.grid()
		self.searchEntry.grid()
		self.searchButton.grid()
		self.listBoxFrame.grid()
		self.yRecordScroll.grid()
		self.recordList.grid()
		self.readOnlyTextFrame.grid()
		self.readOnlyText.grid()
		self.readOnlyTextScroll.grid()
		self.currentScreenName = 'main'

	def draw_edit_record_screen(self):
		if self.currentRecordName == '':
			print("No record to edit!")
		elif self.currentScreenName == 'main':
			self.searchButton.grid_remove()
			self.searchEntry.grid_remove()
			self.searchEntryFrame.grid_remove()
			self.recordList.grid_remove()
			self.yRecordScroll.grid_remove()
			self.listBoxFrame.grid_remove()
			self.listBoxFrame.grid_remove()
			self.readOnlyTextScroll.grid_remove()
			self.readOnlyText.grid_remove()
			self.readOnlyTextFrame.grid_remove()
			self.newAndEditRecordButtonFrame.grid()
			self.editRecordNameLabel.grid()
			self.editRecordNameEntry.grid()
			self.editExistingRecordButton.grid()
			self.cancelEditRecordButton.grid()
			self.editTextFrame.grid()
			self.editText.grid()
			self.editTextScroll.grid()
			self.editRecordName.set(self.currentRecordName)
			self.update_editText(self.app.get_record_data(self.currentRecordName))
			self.currentScreenName ='editrecord'
		else:
			self.leave_add_record_screen()
			self.draw_edit_record_screen()

	def leave_edit_record_screen(self):
		self.editText.delete('1.0','end-1c')
		self.editRecordNameLabel.grid_remove()
		self.editRecordNameEntry.grid_remove()
		self.editExistingRecordButton.grid_remove()
		self.cancelNewRecordButton.grid_remove()
		self.cancelEditRecordButton.grid_remove()
		self.newAndEditRecordButtonFrame.grid_remove()
		self.editTextScroll.grid_remove()
		self.editText.grid_remove()
		self.editTextFrame.grid_remove()
		self.searchEntryFrame.grid()
		self.searchEntry.grid()
		self.searchButton.grid()
		self.listBoxFrame.grid()
		self.yRecordScroll.grid()
		self.recordList.grid()
		self.readOnlyTextFrame.grid()
		self.readOnlyText.grid()
		self.readOnlyTextScroll.grid()
		#self.update_readOnlyText(self.app.get_record_data(self.currentRecordName))
		self.currentRecordName = ''
		self.currentScreenName = 'main'

	def delete_record_request(self):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.delete_record_request.")
		if messagebox.askokcancel("Delete Record","This will permanently delete this record!"):
			self.delete_record_commit()
			feedBackMessage = "Successfully deleted record: " + self.currentRecordName
			self.update_readOnlyText(feedBackMessage)

	def add_record_commit(self):
		# Verify the new record has a name.
		# There should be something besides "" in self.newRecordName
		if self.app.add_record_commit(self.newRecordName.get(), self.editText.get('1.0','end-1c')):
			self.leave_add_record_screen()
			self.query_records()
		else:
			if self.__suppliedOpts.debug:
				print("VerdandiGUI.add_record_commit: app rejected the request.")
			messagebox.showwarning("Duplicate Name", "A record with this name already exists!  Please use a different name.")

	def edit_record_commit(self):
		# Verify the new record has a name.
		# There should be something besides "" in self.newRecordName
		# self.editRecordNameEntry.get()
		if self.app.edit_record_commit(self.currentRecordName, self.editRecordNameEntry.get(), self.editText.get('1.0','end-1c')):
			self.leave_edit_record_screen()
			self.query_records()
		else:
			if self.__suppliedOpts.debug:
				print("VerdandiGUI.edit_record_commit: app rejected the request.")
			messagebox.showwarning("Duplicate Name", "A record with this name already exists!  Please use a different name.")

	def delete_record_commit(self):
		# Verify the new record has a name.
		# There should be something besides "" in self.newRecordName
		self.app.delete_record_commit(self.currentRecordName)
		self.query_records()

	#
	#	Edit Menu Handlers
	#
	def cut_text(self):
		widget = self.focus_get()
		widget.event_generate("<<Cut>>")

	def copy_text(self):
		widget = self.focus_get()
		widget.event_generate("<<Copy>>")

	def paste_text(self):
		widget = self.focus_get()
		widget.event_generate("<<Paste>>")

	def select_all_text(self):
		widget = self.focus_get()
		widget.tag_add(tk.SEL, "1.0", tk.END)
		widget.mark_set(tk.INSERT, "1.0")
		widget.see(tk.INSERT)
		
	def get_random_string(self):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.get_random_string:")
		randomString = self.app.get_random_string()
		self.editText.insert('insert', randomString)
		
		
	def center_window_on_me(self,win):
		self.parent.update_idletasks()
		screenWidth = self.parent.winfo_screenwidth()
		screenHeight = self.parent.winfo_screenheight()
		mainGUIWidth = self.parent.winfo_width()
		mainGUIHeight = self.parent.winfo_height()
		x_pos = self.parent.winfo_x()
		y_pos = self.parent.winfo_y()
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.center_window:")
			print("Monitor Width:", screenWidth)
			print("Monitor Height:", screenHeight)
			print("GUI Width:", mainGUIWidth)
			print("GUI Height:", mainGUIHeight)
			print("X Position:", x_pos)
			print(type(x_pos))
			print("Y Position:", y_pos)
			print(type(y_pos))
		width = win.winfo_width()
		height = win.winfo_height()
		x = int(x_pos + 100)
		y = int(y_pos + 100)
		win.geometry('{}x{}+{}+{}'.format(width, height, x, y))

	#
	#	File Menu Handlers
	#
	def new_sqlite3_database(self):
		newDBDialogRoot = tk.Toplevel()
		newDBDialogRoot.wm_title("New SQLite Database")
		newDBWindow = NewSQLiteDialog(newDBDialogRoot,self,self.__suppliedOpts)
		newDBWindow.pack(side='top', fill='both', expand=True)
		self.center_window_on_me(newDBDialogRoot)
		# Keep the window from shrinking past buttons
		newDBDialogRoot.minsize(300, 150)
		newDBDialogRoot.mainloop()
		
	def new_sqlite3_dialog(self):
		print("naw")

	def connect_database_dialog(self):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.connect_database_dialog")
		self.connectDBDialog = tk.Tk()
		self.connectDBDialog.wm_title("Database Connection")
		dbConnectWindow = ConnectDBDialog(self.connectDBDialog,self,self.__suppliedOpts)
		dbConnectWindow.pack(side='top', fill='both', expand=True)
		self.center_window_on_me(self.connectDBDialog)
		# Keep the window from shrinking past buttons
		#connectDBDialog.update()
		self.connectDBDialog.minsize(300, 100)
		self.connectDBDialog.mainloop()

	#
	#	Text Box Handlers
	#
	def readonly_text_popup(self, event):
		self.readOnlyTextMenu.tk_popup(event.x_root, event.y_root)
		
	def edit_text_popup(self, event):
		self.editTextMenu.tk_popup(event.x_root, event.y_root)

	#
	#	Search Handlers
	#
	def query_records(self):
		# First clean out what is currently in the list box.
		self.recordList.delete(0, 'end')
		# Then get a new list of records from app.
		if self.searchText.get() == '':
			# When no name is supplied, get all the record name.
			if self.__suppliedOpts.debug:
				print("VerdandiGUI.query_records: Executing app.get_all_record_names()")
			topRecordSet = False
			topRecord = ""
			for name in self.app.get_all_record_names():
				self.recordList.insert('end', name)
				if not topRecordSet:
					topRecord = name
					topRecordSet = True
			# Once the listbox is finished being populated, get the first record.
			# Evaluates true if topRecord was actually set
			if not topRecord == "":
				self.update_readOnlyText(self.app.get_record_data(topRecord))
		else:
			# Make a call to app and ask for names like searchText
			if self.__suppliedOpts.debug:
				print("VerdandiGUI.query_records: Executing app.get_names_like('",self.searchText.get(),"')")
			for name in self.app.get_names_like(self.searchText.get()):
				self.recordList.insert('end', name)
		

	#
	#	List Box Handlers
	#
	def execute_selection(self, event):
		widget = event.widget
		selection=widget.curselection()
		self.currentRecordName = widget.get(selection[0])
		self.update_readOnlyText(self.app.get_record_data(self.currentRecordName))

	# -----------------------------------------------------------------
	# INTERNAL METHODS
	# -----------------------------------------------------------------
	def update_readOnlyText(self,newText):
		self.readOnlyText.delete('1.0','end-1c')
		self.readOnlyText.insert('end', newText)

	def update_editText(self,newText):
		self.editText.delete('1.0','end-1c')
		self.editText.insert('end', newText)
		
	def check_passphrase(self,passphrase):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.compare_passphrase")
		if self.app.check_passphrase(passphrase):
			self.passwdDialogRoot.destroy()
			self.app.copy_db_to_memdb()
			self.query_records()
		else:
			self.keyEntry.delete(0,'end')

	def init_passphrase(self, passphrase, confirm):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.init_passphrase")
		if passphrase == confirm:
			if self.app.change_key(passphrase):
				self.changePasswdDialogRoot.destroy()
		else:
			self.newPhraseEntry.delete(0,'end')
			self.confirmEntry.delete(0,'end')

	def change_passphrase(self, oldPhrase, passphrase, confirm):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.change_passphrase")
		if self.app.check_passphrase(oldPhrase):
			self.init_passphrase(passphrase, confirm)
		else:
			self.currentPhraseEntry.delete(0,'end')
			self.newPhraseEntry.delete(0,'end')
			self.confirmEntry.delete(0,'end')

	def password_dialog(self):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.password_dialog")
		self.passwdDialogRoot = tk.Toplevel(self.parent,takefocus=True)
		self.passwdDialogRoot.attributes("-topmost", True)
		self.passwdDialogRoot.wm_title("Passphrase")
		self.passwordFrame = tk.Frame(self.passwdDialogRoot, borderwidth=0,
											background=self.backColor,
											highlightbackground=self.noFocusColor, highlightcolor=self.noFocusColor,
											highlightthickness=1)
		self.passwordFrame.grid(row=0, column=0, sticky='n'+'s'+'w'+'e')
		self.passwordLabel = tk.Label(self.passwordFrame, text="Key:", font=self.font,
											background=self.backColor, foreground=self.foreColor,
											highlightbackground=self.noFocusColor, highlightcolor=self.noFocusColor,
											highlightthickness=1)
		self.passwordLabel.grid(row=0, column=0, sticky='n')
		self.keyEntry = tk.Entry(self.passwordFrame, width=20, show='*',
										background=self.backColor, foreground=self.foreColor,
										insertbackground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,)
		self.keyEntry.grid(row=0, column=1, sticky='w'+'e')
		self.buttonFrame = tk.Frame(self.passwdDialogRoot, borderwidth=0,
											background=self.backColor,
											highlightbackground=self.noFocusColor, highlightcolor=self.noFocusColor,
											highlightthickness=1)
		self.buttonFrame.grid(row=1, column=0,	sticky='n'+'s'+'w'+'e')
		self.buttonFrame.grid_columnconfigure(0, weight=1)
		self.cancelButton = tk.Button(self.buttonFrame, text="Cancel", font=self.font, borderwidth=0,
										activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
										background=self.backColor, foreground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,
										command=lambda: self.passwdDialogRoot.destroy())
		self.cancelButton.bind("<Return>",lambda event: self.passwdDialogRoot.destroy())
		self.cancelButton.grid(row=0, column=0, sticky='s'+'w')
		self.confirmButton = tk.Button(self.buttonFrame, text="Confirm", font=self.font, borderwidth=0,
										activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
										background=self.backColor, foreground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,
										command=lambda: self.check_passphrase(self.keyEntry.get()))
		self.keyEntry.bind("<Return>",lambda event: self.check_passphrase(self.keyEntry.get()))
		self.confirmButton.bind("<Return>",lambda event: self.check_passphrase(self.keyEntry.get()))
		self.confirmButton.grid(row=0, column=1, sticky='s'+'e')
		self.passwdDialogRoot.update()
		myGUIWidth = self.passwdDialogRoot.winfo_width()
		myGUIHeight = self.passwdDialogRoot.winfo_height()
		if self.__suppliedOpts.debug:
			print("Monitor Width:", self.screenWidth)
			print("Monitor Height:", self.screenHeight)
			print("GUI Width:", myGUIWidth)
		x = int(self.screenWidth / 2)
		y = int(self.screenHeight / 2)
		self.passwdDialogRoot.geometry('{}x{}+{}+{}'.format(myGUIWidth, myGUIHeight, x, y))
		self.passwdDialogRoot.after(500, lambda: self.passwdDialogRoot.focus_force())
		self.keyEntry.focus_force()
		
	def change_passphrase_dialog(self,changeExisting):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.change_password_dialog")
		self.changePasswdDialogRoot = tk.Toplevel(self.parent,takefocus=True)
		self.changePasswdDialogRoot.attributes("-topmost", True)
		self.changePasswdDialogRoot.wm_title("Change Passphrase")
		self.passwordFrame = tk.Frame(self.changePasswdDialogRoot, borderwidth=0,
										background=self.backColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.noFocusColor,
										highlightthickness=1)
		self.passwordFrame.grid(row=0, column=0, sticky='n'+'s'+'w'+'e')
		passwordFrameRow = 0
		if changeExisting:
			self.currentPhraseLabel = tk.Label(self.passwordFrame, text="Current Passphrase:", font=self.font,
										background=self.backColor, foreground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.noFocusColor,
										highlightthickness=1)
			self.currentPhraseLabel.grid(row=passwordFrameRow, column=0, sticky='n')
			self.currentPhraseEntry = tk.Entry(self.passwordFrame, width=20, show='*',
										background=self.backColor, foreground=self.foreColor,
										insertbackground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,)
			self.currentPhraseEntry.grid(row=passwordFrameRow, column=1, sticky='w'+'e')
			passwordFrameRow += 1
		self.newPhraseLabel = tk.Label(self.passwordFrame, text="New Passphrase:", font=self.font,
										background=self.backColor, foreground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.noFocusColor,
										highlightthickness=1)
		self.newPhraseLabel.grid(row=passwordFrameRow, column=0, sticky='n')
		self.newPhraseEntry = tk.Entry(self.passwordFrame, width=20, show='*',
										background=self.backColor, foreground=self.foreColor,
										insertbackground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,)
		self.newPhraseEntry.grid(row=passwordFrameRow, column=1, sticky='w'+'e')
		passwordFrameRow += 1
		self.confirmLabel = tk.Label(self.passwordFrame, text="Confirm Passphrase:", font=self.font,
										background=self.backColor, foreground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.noFocusColor,
										highlightthickness=1)
		self.confirmLabel.grid(row=passwordFrameRow, column=0, sticky='n')
		self.confirmEntry = tk.Entry(self.passwordFrame, width=20, show='*',
										background=self.backColor, foreground=self.foreColor,
										insertbackground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,)
		self.confirmEntry.grid(row=passwordFrameRow, column=1, sticky='w'+'e')
		
		self.buttonFrame = tk.Frame(self.changePasswdDialogRoot, borderwidth=0,
										background=self.backColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.noFocusColor,
										highlightthickness=1)
		self.buttonFrame.grid(row=1, column=0,	sticky='n'+'s'+'w'+'e')
		self.buttonFrame.grid_columnconfigure(0, weight=1)
		self.cancelButton = tk.Button(self.buttonFrame, text="Cancel", font=self.font, borderwidth=0,
										activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
										background=self.backColor, foreground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,
										command=lambda: self.changePasswdDialogRoot.destroy())
		self.cancelButton.bind("<Return>",lambda event: self.changePasswdDialogRoot.destroy())
		self.cancelButton.grid(row=0, column=0, sticky='s'+'w')
		if changeExisting:
			self.confirmButton = tk.Button(self.buttonFrame, text="Confirm", font=self.font, borderwidth=0,
											activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
											background=self.backColor, foreground=self.foreColor,
											highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
											highlightthickness=1,
											command=lambda: self.change_passphrase(self.currentPhraseEntry.get(),
																					self.newPhraseEntry.get(),
																					self.confirmEntry.get()))
			self.confirmButton.bind("<Return>",lambda event: self.change_passphrase(self.currentPhraseEntry.get(),
																					self.newPhraseEntry.get(),
																					self.confirmEntry.get()))
			self.currentPhraseEntry.bind("<Return>",lambda event: self.change_passphrase(self.currentPhraseEntry.get(),
																					self.newPhraseEntry.get(),
																					self.confirmEntry.get()))
			self.newPhraseEntry.bind("<Return>",lambda event: self.change_passphrase(self.currentPhraseEntry.get(),
																					self.newPhraseEntry.get(),
																					self.confirmEntry.get()))
			self.confirmEntry.bind("<Return>",lambda event: self.change_passphrase(self.currentPhraseEntry.get(),
																					self.newPhraseEntry.get(),
																					self.confirmEntry.get()))
		else:
			self.confirmButton = tk.Button(self.buttonFrame, text="Confirm", font=self.font, borderwidth=0,
										activebackground=self.activeBGColor, activeforeground=self.activeFGColor,
										background=self.backColor, foreground=self.foreColor,
										highlightbackground=self.noFocusColor, highlightcolor=self.focusColor,
										highlightthickness=1,
										command=lambda: self.init_passphrase(self.newPhraseEntry.get(),self.confirmEntry.get()))
			self.confirmButton.bind("<Return>",lambda event: self.init_passphrase(self.newPhraseEntry.get(),self.confirmEntry.get()))
			self.newPhraseEntry.bind("<Return>",lambda event: self.init_passphrase(self.newPhraseEntry.get(),self.confirmEntry.get()))
			self.confirmEntry.bind("<Return>",lambda event: self.init_passphrase(self.newPhraseEntry.get(),self.confirmEntry.get()))
		self.confirmButton.grid(row=0, column=1, sticky='s'+'e')
		self.changePasswdDialogRoot.update()
		myGUIWidth = self.changePasswdDialogRoot.winfo_width()
		myGUIHeight = self.changePasswdDialogRoot.winfo_height()
		if self.__suppliedOpts.debug:
			print("Monitor Width:", self.screenWidth)
			print("Monitor Height:", self.screenHeight)
			print("GUI Width:", myGUIWidth)
		x = int(self.screenWidth / 2)
		y = int(self.screenHeight / 2)
		self.changePasswdDialogRoot.geometry('{}x{}+{}+{}'.format(myGUIWidth, myGUIHeight, x, y))
		self.changePasswdDialogRoot.after(500, lambda: self.changePasswdDialogRoot.focus_force())
		
	def change_passphrase_button(self):
		# Verify there is a passphrase to change
		if self.app.check_passphrase(""):
			print("No password set in database")
			self.change_passphrase_dialog(False)
		else:
			print("This will be a change")
			self.change_passphrase_dialog(True)
			
	def connect_database(self,dbmsType,dbmsPath):
		if self.__suppliedOpts.debug:
			print("VerdandiGUI.connect_database(",dbmsType,dbmsPath,")")
		if dbmsType == "sqlite3":
			if dbmsPath != "":
				self.app.config_change_sqlite3_path(dbmsPath)
				self.init_app()
				self.connectDBDialog.destroy()
				if self.app.check_passphrase(""):
					self.app.copy_db_to_memdb()
					# Populate the list Box
					self.query_records()
				else:
					self.password_dialog()
			


#EOF
