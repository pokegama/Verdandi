#!/usrb/bin/env python3
#                                                            05-MAR-2017
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox

class NewSQLiteDialog(tk.Frame):
	"""Defines the GUI"""
	def __init__(self, parent, grandparent, suppliedOpts):
		self.parent = parent
		self.grandparent = grandparent
		self.__suppliedOpts = suppliedOpts
		self.dbmsSelection = "sqlite3"
		self.encryptionState = tk.StringVar(master=parent)
		self.encryptionState.set("Plaintext")

		# parent Frame ------------------------------------------------
		tk.Frame.__init__(self, parent, borderwidth=1)
		# These help make the window stretchy
		self.grid_columnconfigure(0, weight=1)
		self.grid_rowconfigure(0, weight=1)
		self.init_GUI()
		self.toggle_key_entry_off()

	def init_GUI(self):
		self.newDBDialogFrame = tk.Frame(self, borderwidth=1, relief='raised')
		self.newDBDialogFrame.grid(row=0, column=0,	sticky='n'+'s'+'w'+'e')
		self.newDBDialogFrame.grid_rowconfigure(3, weight=1)
		self.newDBDialogFrame.grid_columnconfigure(0, weight=1)
		#	dbTypeFrame
		self.dbTypeFrame = tk.Frame(self.newDBDialogFrame, borderwidth=1, relief='raised')
		self.dbTypeFrame.grid(row=0, column=0,	sticky='n'+'w'+'e')
		self.dbTypeFrame.grid_columnconfigure(0, weight=1)
		self.dbTypeLabel = tk.Label(self.dbTypeFrame, text="SQLite3")
		self.dbTypeLabel.grid(row=0, column=0, sticky='n')
		#	encryptionFrame
		self.encryptionFrame = tk.Frame(self.newDBDialogFrame, borderwidth=1, relief='raised')
		self.encryptionFrame.grid(row=1, column=0,	sticky='n'+'w'+'e')
		self.encryptionOff = tk.Radiobutton(self.encryptionFrame,
														text="Plaintext",
														indicatoron = 0,
														variable=self.encryptionState,
														value="Plaintext",
														command=self.toggle_key_entry_off)
		self.encryptionOff.grid(row=0, column=0, sticky='n'+'w')
		self.encryptionOn = tk.Radiobutton(self.encryptionFrame,
														text="Encrypted",
														indicatoron = 0,
														variable=self.encryptionState,
														value="Encrypted",
														command=self.toggle_key_entry_on)
		self.encryptionOn.grid(row=0, column=1, sticky='n'+'w')
		#	keyEntryFrame
		self.keyEntryFrame = tk.Frame(self.newDBDialogFrame, borderwidth=1, relief='raised')
		self.keyEntryFrame.grid(row=2, column=0,	sticky='n'+'w'+'e')
		self.enterKeyLabel = tk.Label(self.keyEntryFrame, text="Enter Key:")
		self.enterKeyLabel.grid(row=1, column=0, sticky='w')
		self.keyEntry = tk.Entry(self.keyEntryFrame, width=20, show='*')
		self.keyEntry.grid(row=1, column=1, sticky='w'+'e')
		self.confirmKeyLabel = tk.Label(self.keyEntryFrame, text="Confirm Key:")
		self.confirmKeyLabel.grid(row=2, column=0, sticky='w')
		self.keyConfirm = tk.Entry(self.keyEntryFrame, width=20, show='*')
		self.keyConfirm.grid(row=2, column=1, sticky='w'+'e')
		#	pathFrame
		self.pathFrame = tk.Frame(self.newDBDialogFrame, borderwidth=1, relief='raised')
		self.pathFrame.grid(row=3, column=0,	sticky='n'+'w'+'e')
		self.pathFrame.grid_columnconfigure(1, weight=1)
		self.savePathLabel = tk.Label(self.pathFrame, text="Path:")
		self.savePathLabel.grid(row=0, column=0, sticky='w')
		self.savePathEntry = tk.Entry(self.pathFrame, width=0)
		self.savePathEntry.grid(row=0, column=1, sticky='w'+'e')
		self.savePathButton = tk.Button(self.pathFrame, text=". . .",
										font=("Helvatica", 3, "bold"),
										command=self.saveFileAs)
		self.savePathButton.grid(row=0, column=2, sticky='w')
		#	cancelCreateFrame
		self.cancelCreateFrame = tk.Frame(self.newDBDialogFrame, borderwidth=1, relief='raised')
		self.cancelCreateFrame.grid(row=4, column=0,	sticky='s'+'w'+'e')
		self.cancelCreateFrame.grid_columnconfigure(1, weight=1)
		#	Cancel Button
		self.cancelButton = tk.Button(self.cancelCreateFrame, text="Cancel",
										font=("Helvatica", 7, "bold"),
										command=lambda: self.master.destroy())
		self.cancelButton.grid(row=0, column=0, sticky='s'+'w')
		#	Create Button
		self.createButton = tk.Button(self.cancelCreateFrame, text="Create",
										font=("Helvatica", 7, "bold"),
										command=self.create_new_database)
		self.createButton.grid(row=0, column=1, sticky='s'+'e')
		
	def toggle_key_entry_on(self):
		self.keyEntry.configure(state='normal')
		self.keyConfirm.configure(state='normal')
		
	def toggle_key_entry_off(self):
		self.keyEntry.delete(0,'end')
		self.keyEntry.configure(state='disabled')
		self.keyConfirm.configure(state='disabled')
		
	def compare_keys(self):
		if self.__suppliedOpts.debug:
				print("NewSQLiteDialog.compare_keys()")
		if self.encryptionState.get() == "Encrypted":
			if self.keyEntry.get() == self.keyConfirm.get():
				return True
			else:
				if self.__suppliedOpts.debug:
					print("NewSQLiteDialog.compare_keys: Keys don't match.")
				messagebox.showwarning("Key Warning", "The values in 'Enter Key' and 'Confrim Key' must match.")
				self.parent.lift()
				return False
		else:
			return True
			
	def verify_path(self):
		if self.savePathEntry.get() == "":
			if self.__suppliedOpts.debug:
				print("NewSQLiteDialog.create_new_database: savePathEntry is empty.")
			messagebox.showwarning("Path Warning", "No path was provided.")
			self.parent.lift()
			return False
		else:
			return True

	def saveFileAs(self):
		pathstring = filedialog.asksaveasfilename(defaultextension='.db',
												filetypes=(("SQLite Databases",".db"),
												("All files", "*")))
		self.savePathEntry.delete(0,tk.END)
		self.savePathEntry.insert(0, pathstring)
		self.parent.lift()

	def create_new_database(self):
		if self.__suppliedOpts.debug:
			print("NewSQLiteDialog.create_new_database: grandparent.app.create_new_database()")
			print("NewSQLiteDialog.create_new_database: dbms is", self.dbmsSelection)
			print("NewSQLiteDialog.create_new_database: dbName is", self.savePathEntry.get())
			print("NewSQLiteDialog.create_new_database: keyEntry is", self.keyEntry.get())
		if self.compare_keys():
			if self.verify_path():
				self.grandparent.app.create_new_database(self.dbmsSelection,self.savePathEntry.get(),self.keyEntry.get())
				self.grandparent.app.config_change_sqlite3_path(self.savePathEntry.get())
				self.grandparent.app.init_app()
				self.grandparent.query_records()
				self.master.destroy()
			
			
			
			


#EOF
