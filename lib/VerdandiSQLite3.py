#!/usr/bin/env python3
#                                                            25-FEB-2017
#  VerdandiSQLite.py
#
#  The interface for SQLite databases

import sqlite3


class VerdandiSQLite3:
	def __init__(self, parent,suppliedOpts,dbFullPath):
		self.__suppliedOpts = suppliedOpts
		self.__dbFullPath = dbFullPath
		self.parent = parent
		self.allRecordNames = []
		self.init_sqlite3()

	def init_sqlite3(self):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.init_sqlite3: __dbFullPath", self.__dbFullPath)
			
	def get_all_records(self):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.get_all_records('SELECT ID,Name,Data,TransactionDate FROM data_blobs')")
		thisQuery = []
		conn = sqlite3.connect(self.__dbFullPath)
		c = conn.cursor()
		for record in c.execute("SELECT ID,Name,Data,TransactionDate FROM data_blobs"):
			thisQuery.append(record)
		conn.close()
		return thisQuery

	def create_new_db(self,newDBPath,maskedKeyAndSalt):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.create_new_db", newDBPath)
		if newDBPath == "":
			newDBPath = self.__dbFullPath
		conn = sqlite3.connect(newDBPath)
		c = conn.cursor()
		c.execute('''CREATE TABLE data_blobs
		            (ID varchar(36) PRIMARY KEY NOT NULL,
		            Name BLOB,
		            Data BLOB,
		            TransactionDate BLOB)''')
		c.execute('''CREATE TABLE hashed_key
		            (key BLOB,
		            salt BLOB)''')
		c.execute("INSERT INTO hashed_key (key,salt) VALUES (?,?)", maskedKeyAndSalt)
		conn.commit()
		conn.close()
		print("Created new database at: ",  newDBPath)

	def add_record_commit(self, recordID, recordName, textData, transactionDatetime):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.add_record_commit('INSERT INTO data_blobs(ID, Name, Data, TransactionDate) VALUES (?,?,?,?)')", recordID)
		conn = sqlite3.connect(self.__dbFullPath)
		c = conn.cursor()
		c.execute("INSERT INTO data_blobs(ID, Name, Data, TransactionDate) VALUES (?,?,?,?)", (recordID, recordName, textData, transactionDatetime))
		conn.commit()
		conn.close()

	def edit_record_commit(self, recordID, recordName, textData, transactionDatetime):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.edit_record_commit('UPDATE data_blobs SET Data = :textData WHERE ID = :recordID", recordID)
		conn = sqlite3.connect(self.__dbFullPath)
		c = conn.cursor()
		c.execute("UPDATE data_blobs SET Name = :recordName, Data = :textData, TransactionDate = :transactionDatetime WHERE ID = :recordID", {"recordID": recordID, "recordName": recordName, "textData": textData, "transactionDatetime": transactionDatetime})
		conn.commit()
		conn.close()

	def delete_record_commit(self, recordID):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.delete_record_commit('DELETE FROM data_blobs WHERE ID = :recordID", recordID)
		conn = sqlite3.connect(self.__dbFullPath)
		c = conn.cursor()
		c.execute("DELETE FROM data_blobs WHERE ID = :recordID", {"recordID": recordID})
		conn.commit()
		conn.close()
		
	def get_hashed_key(self):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.get_hashed_key")
		thisQuery = []
		conn = sqlite3.connect(self.__dbFullPath)
		c = conn.cursor()
		for key in c.execute("SELECT key FROM hashed_key"):
			thisQuery.append(key[0])
		conn.close()
		return thisQuery[0]
		
	def get_maskedKey_and_salt(self):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.get_maskedKey_and_salt")
		thisQuery = []
		conn = sqlite3.connect(self.__dbFullPath)
		c = conn.cursor()
		for key,salt in c.execute("SELECT key,salt FROM hashed_key"):
			thisQuery.append(key)
			thisQuery.append(salt)
		conn.close()
		return thisQuery
		
	def update_maskedKey_and_salt(self,maskedKeyAndSalt):
		if self.__suppliedOpts.debug:
			print("VerdandiSQLite3.update_maskedKey_and_salt")
		conn = sqlite3.connect(self.__dbFullPath)
		c = conn.cursor()
		c.execute("DELETE FROM hashed_key")
		c.execute("INSERT INTO hashed_key (key,salt) VALUES (?,?)", maskedKeyAndSalt)
		conn.commit()
		conn.close()
		return True

