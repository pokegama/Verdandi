##! /usr/bin/env python3
from tkinter import Text


class ReadOnlyText(Text):
    tagInit = False
    # This is the list of all default command in the "Text" tag that modify the text
    commandsToRemove = (
    "<Control-Key-h>",
    "<Meta-Key-Delete>",
    "<Meta-Key-BackSpace>",
    "<Meta-Key-d>",
    "<Meta-Key-b>",
    "<<Redo>>",
    "<<Undo>>",
    "<Control-Key-t>",
    "<Control-Key-o>",
    "<Control-Key-k>",
    "<Control-Key-d>",
    "<Key>",
    "<Key-Insert>",
    "<<PasteSelection>>",
    "<<Clear>>",
    "<<Paste>>",
    "<<Cut>>",
    "<Key-BackSpace>",
    "<Key-Delete>",
    "<Key-Return>",
    "<Control-Key-i>",
    "<Key-Tab>",
    "<Shift-Key-Tab>"
    )

    def init_tag(self):
        for key in self.bind_class("Text"):
            if key not in ReadOnlyText.commandsToRemove:
                command = self.bind_class("Text", key)
                self.bind_class("ReadOnlyText", key, command)
        ReadOnlyText.tagInit = True


    def __init__(self, *args, **kwords):
        Text.__init__(self, *args, **kwords)
        if not ReadOnlyText.tagInit:
            self.init_tag()

        # Create a new binding table list, replace the default Text binding table by the ReadOnlyText one
        bindTags = tuple(tag if tag!="Text" else "ReadOnlyText" for tag in self.bindtags())
        self.bindtags(bindTags)
