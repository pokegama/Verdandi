#!/usr/bin/env python3
#                                                            05-MAR-2017
#  VerdandiApp.py
#
#  The Verdandi application

import sqlite3
import uuid
from datetime import datetime
import platform
from os import makedirs
import os.path
import configparser
from lib.VerdandiSQLite3 import VerdandiSQLite3
from lib.VerdandiSkein import VerdandiSkein

class VerdandiApp:
    def __init__(self, parent, suppliedOpts):
        self.__suppliedOpts = suppliedOpts
        self.parent = parent
        self.memdb = ""
        self.verdandiHome = ""
        self.configFilePath = ""
        self.detectedOS = ""
        self.config = configparser.ConfigParser()
        self.theme = configparser.ConfigParser()
        self.dbms = ""
        self.db = ""
        self.skein = ""
        self.init_app()

    def init_app(self):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.init_app: Doing init stuff.")
        # Initialize app DB in memory
        self.init_mem_db()
        # Determine which OS is being used
        self.get_platform()
        self.init_skein()
        # Get configuration file, and configure some of the app
        # properties for this session
        self.get_config()
        self.dbms = self.config['MAIN']['dbms']
        if self.__suppliedOpts.debug:
            print("VerdandiApp.init_app: dbms is", self.dbms)
        self.init_db()
        
    def init_mem_db(self):
        self.memdb = sqlite3.connect(':memory:')
        c = self.memdb.cursor()
        c.execute('''CREATE TABLE data_blobs
                    (ID varchar(36) PRIMARY KEY NOT NULL,
                    Name BLOB,
                    Data BLOB,
                    TransactionDate BLOB)''')
        c.execute('''CREATE TABLE hashed_key
                    (key BLOB,
                    salt BLOB)''')

    def get_platform(self):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.get_platform: Determine OS.")
        if not platform.system():
            # If this evaluates True, then the OS type (windows, linux,
            # java...) couldn't be determined.  Not off to good start..
            if self.__suppliedOpts.debug:
                print("VerdandiApp.get_platform: Unable to determine OS.")
            exit("00000F50") # ;)
        else:
            if self.__suppliedOpts.debug:
                print("VerdandiApp.get_platform: OS is", platform.system())
            self.detectedOS = platform.system()

    def verify_verdandi_dirs(self):
        defaultDBDir = self.verdandiHome + "db"
        defaultThemeDir = self.verdandiHome + "themes"
        if self.__suppliedOpts.debug:
            print("VerdandiApp.verify_verdandi_dirs: Verifying Verdandi environment is setup...")
        if os.path.exists(self.verdandiHome):
            if not os.path.exists(defaultDBDir):
                if self.__suppliedOpts.debug:
                    print("VerdandiApp.verify_verdandi_dirs: Verdandi db directory doesn't exist; Creating it.")
                feedbackmessage = "Verdandi db directory doesn't exist; Creating it."
                os.makedirs(defaultDBDir)
            if not os.path.exists(defaultThemeDir):
                if self.__suppliedOpts.debug:
                    print("VerdandiApp.verify_verdandi_dirs: Verdandi themes directory doesn't exist; Creating it.")
                feedbackmessage = "Verdandi themes directory doesn't exist; Creating it."
                os.makedirs(defaultThemeDir)
        else:
            feedbackmessage = "Verdandi home doesn't exist; Creating it."
            os.makedirs(defaultDBDir)
            os.makedirs(defaultThemeDir)

    def create_default_config(self):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.create_default_config: Creating default config file.")
        if self.detectedOS == "Linux":
            self.verify_verdandi_dirs()
            defaultDBPath = self.verdandiHome + "db/MyVerdandiDB.db"
            defaultThemePath = self.verdandiHome + "themes/default.conf"
            self.config['MAIN'] = {'dbms': 'sqlite3','theme': defaultThemePath}
            self.config['SQLITE'] = {'db_file': defaultDBPath}
            with open(self.configFilePath, 'w') as configFile:
                self.config.write(configFile)
            self.theme['COLORS'] = {'background': '#2E3436','foreground': '#E9B96E','focus': '#586E75','textforeground': '#990000','textbackground': '#000000'}
            self.theme['FONT'] = {'name': 'Helvatica', 'size': '11', 'style': 'normal'}
            with open(defaultThemePath, 'w') as themeFile:
                self.theme.write(themeFile)
        if self.detectedOS == "Windows":
            self.verify_verdandi_dirs()
            defaultDBPath = self.verdandiHome + "db\\MyVerdandiDB.db"
            defaultThemePath = self.verdandiHome + "themes\\default.conf"
            self.config['MAIN'] = {'dbms': 'sqlite3','theme': defaultThemePath}
            self.config['SQLITE'] = {'db_file': defaultDBPath}
            with open(self.configFilePath, 'w') as configFile:
                self.config.write(configFile)
            self.theme['COLORS'] = {'background': '#2E3436','foreground': '#E9B96E','focus': '#586E75','textforeground': '#990000','textbackground': '#000000'}
            self.theme['FONT'] = {'name': 'Helvatica', 'size': '11', 'style': 'normal'}
            with open(defaultThemePath, 'w') as themeFile:
                self.theme.write(themeFile)
        
    def get_config(self):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.get_config: Checking for config file.")
        if self.detectedOS == "Linux":
            self.verdandiHome = os.path.expanduser("~") + "/.verdandi/"
            self.configFilePath = self.verdandiHome + "verdandi.conf"
            if os.path.exists(self.configFilePath):
                if self.__suppliedOpts.debug:
                    print("VerdandiApp.get_config: Config file found.")
                # Pull in the configuration so it can be acted upon.
                self.config.read(self.configFilePath)
                self.theme.read(self.config['MAIN']['theme'])
            else:
                # If there isn't a configuration to act on, setup the
                # default one to get things rolling along.
                if self.__suppliedOpts.debug:
                    print("VerdandiApp.get_config: Config file NOT found!")
                    print("VerdandiApp.get_config: Creating one at:", self.configFilePath)
                self.create_default_config()
                self.config.read(self.configFilePath)
        elif self.detectedOS == "Windows":
            self.verdandiHome = os.path.expanduser("~") + "\\AppData\\Roaming\\verdandi\\"
            self.configFilePath = self.verdandiHome + "verdandi.conf"
            if os.path.exists(self.configFilePath):
                if self.__suppliedOpts.debug:
                    print("VerdandiApp.get_config: Config file found.")
                # Pull in the configuration so it can be acted upon.
                self.config.read(self.configFilePath)
                self.theme.read(self.config['MAIN']['theme'])
            else:
                # If there isn't a configuration to act on, setup the
                # default one to get things rolling along.
                if self.__suppliedOpts.debug:
                    print("VerdandiApp.get_config: Config file NOT found!")
                    print("VerdandiApp.get_config: Creating one at:", self.configFilePath)
                self.create_default_config()
                self.config.read(self.configFilePath)
            if self.__suppliedOpts.debug:
                print("VerdandiApp.get_config: Home is at " + self.verdandiHome)
                #exit()
        else:
            if self.__suppliedOpts.debug:
                print("VerdandiApp.get_config: Development required to handle this OS.")
            exit("00000001")

    def config_change_sqlite3_path(self,newDBPath):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.config_change_sqlite3_path: Changing config [SQLITE].")
        self.config['SQLITE'] = {'db_file': newDBPath}
        with open(self.configFilePath, 'w') as configFile:
            self.config.write(configFile)

    def create_new_database(self, dbms, dbName, passphrase):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.create_new_database: ", dbms, dbName, passphrase)
        if len(passphrase) == 0:
            maskedKeyAndSalt = ("","")
        else:
            # Initialize the VerdandiSkein object with this passphrase
            self.skein.init_key(passphrase)
            # This will return a tuple of the masked key from pbkdf2_hmac and the salt
            maskedKeyAndSalt = self.skein.get_maskedKey_and_salt()
        if dbms == "sqlite3":
            print("Making a call to self.db.create_new_db(dbName)")
            self.db.create_new_db(dbName, maskedKeyAndSalt)
        else:
            print("Further development required to create dbms type:", dbms)

    def get_all_record_names(self):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.get_all_record_names('SELECT ID,Name FROM data_blobs ORDER BY Name')")
        thisQuery = []
        #conn = sqlite3.connect(self.__dbFullPath)
        c = self.memdb.cursor()
        for recID, name in c.execute("SELECT ID,Name FROM data_blobs ORDER BY Name"):
            thisQuery.append(name)
        return thisQuery
            
    def get_record_data(self, name):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.get_record_data.", name)
        thisQuery = []
        c = self.memdb.cursor()
        for name, date in c.execute("SELECT Data,TransactionDate FROM data_blobs WHERE Name=:name", {"name": name}):
            thisQuery.append(name)
        return thisQuery[0]
        
    def get_names_like(self, searchText):
        # Get records that have a Name like searchText
        if self.__suppliedOpts.debug:
            print("VerdandiApp.get_names_like.", searchText)
        thisQuery = []
        c = self.memdb.cursor()
        for recID, name in c.execute("SELECT ID,Name FROM data_blobs WHERE Name LIKE ?", ('%'+searchText+'%',)):
            thisQuery.append(name)
        if self.__suppliedOpts.debug:
                print("VerdandiApp.get_names_like. Name LIKE Search Results:", thisQuery)
        if thisQuery == []:
            for recID, name in c.execute("SELECT ID, Name FROM data_blobs WHERE Data LIKE ?", ('%'+searchText+'%',)):
                thisQuery.append(name)
            if self.__suppliedOpts.debug:
                print("VerdandiApp.get_names_like. Data LIKE Search Results:", thisQuery)
            return thisQuery
        else:
            return thisQuery
            
    def duplicate_record_name_check(self, recordName):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.duplicate_record_name_check.", recordName)
        c = self.memdb.cursor()
        # Verify a record with that name doesn't already exist.
        thisQuery = []
        for recID, name in c.execute("SELECT ID,Name FROM data_blobs WHERE Name=:recordName", {"recordName": recordName}):
            thisQuery.append(name)
        if not thisQuery:
            # True if the resultset is empty
            return True
        else:
            return False

    def add_record_commit(self, recordName, textData):
        # Verify the new record has a name.
        # There should be something besides "" in self.newRecordName
        if self.__suppliedOpts.debug:
            print("VerdandiApp.add_record_commit.", recordName)
        c = self.memdb.cursor()
        # Verify a record with that name doesn't already exist.
        thisQuery = []
        if self.duplicate_record_name_check(recordName):
            recordID = str(uuid.uuid4())
            transactionDatetime = datetime.now().__format__("%y-%m-%d %H:%M:%S")
            c.execute("INSERT INTO data_blobs(ID, Name, Data, TransactionDate) VALUES (?,?,?,?)", (recordID, recordName, textData, transactionDatetime))
            if self.skein.key == "":
                # Write data to DB as plaintext
                self.db.add_record_commit(recordID, recordName, textData, transactionDatetime)
                return True
            else:
                # Otherwise, encrypt it first
                encryptedName = self.skein.encrypt_string(recordName)
                encryptedText = self.skein.encrypt_string(textData)
                encryptedDate = self.skein.encrypt_string(transactionDatetime)
                self.db.add_record_commit(recordID, encryptedName, encryptedText, encryptedDate)
                return True
        else:
            return False
    
    def update_existing_record(self, recordName, newName, textData):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.edit_record_commit.", recordName)
        transactionDatetime = datetime.now().__format__("%y-%m-%d %H:%M:%S")
        c = self.memdb.cursor()
        thisQuery = []
        for recID, name in c.execute("SELECT ID,Name FROM data_blobs WHERE Name=:recordName", {"recordName": recordName}):
            thisQuery.append(recID)
            recordID = thisQuery[0]
        c.execute("UPDATE data_blobs SET Name = :newName, Data = :textData, TransactionDate = :transactionDatetime WHERE ID = :recordID", {"recordID": recordID, "newName": newName, "textData": textData, "transactionDatetime": transactionDatetime})
        if self.skein.key == "":
            # Write data to DB as plaintext
            self.db.edit_record_commit(recordID, newName, textData, transactionDatetime)
            return True
        else:
            # Otherwise, encrypt it first
            encryptedName = self.skein.encrypt_string(newName)
            encryptedText = self.skein.encrypt_string(textData)
            encryptedDate = self.skein.encrypt_string(transactionDatetime)
            self.db.edit_record_commit(recordID, encryptedName, encryptedText, encryptedDate)
            return True
        
    def edit_record_commit(self, recordName, newName, textData):
        # Verify the new record has a name.
        # There should be something besides "" in self.newRecordName
        if self.__suppliedOpts.debug:
            print("VerdandiApp.edit_record_commit.", recordName)
        # If the name has been changed during the edit, verify it isn't a duplicate
        if newName == recordName:
            if self.update_existing_record(recordName, newName, textData):
                return True
            else:
                return False
        else:
            if self.duplicate_record_name_check(newName):
                if self.update_existing_record(recordName, newName, textData):
                    return True
                else:
                    return False
            else:
                return False

    def delete_record_commit(self, recordName):
        # Verify the new record has a name.
        # There should be something besides "" in self.newRecordName
        if self.__suppliedOpts.debug:
            print("VerdandiApp.delete_record_commit.", recordName)
        thisQuery = []
        c = self.memdb.cursor()
        for recID, name in c.execute("SELECT ID,Name FROM data_blobs WHERE Name=:recordName", {"recordName": recordName}):
            thisQuery.append(recID)
        recordID = thisQuery[0]
        c.execute("DELETE FROM data_blobs WHERE ID = :recordID", {"recordID": recordID})
        self.db.delete_record_commit(recordID)
        
    def check_passphrase(self,passphrase):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.check_passphrase.")
        maskedKeyAndSalt = self.db.get_maskedKey_and_salt()
        maskedKeyFromDB = maskedKeyAndSalt[0]
        salt = maskedKeyAndSalt[1]
        if maskedKeyFromDB == "":
            return True
        elif self.skein.check_passphrase(passphrase, maskedKeyFromDB, salt):
            return True
        else:
            return False
        
    def copy_db_to_memdb(self):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.unlock.")
        dataBlobRecords = self.db.get_all_records()
        c = self.memdb.cursor()
        if self.skein.key == "":
            for record in dataBlobRecords:
                # Name,Data,Checksum,TransactionDate
                recordID = record[0]
                recordName = record[1]
                recordData = record[2]
                recordDate = record[3]
                c.execute("INSERT INTO data_blobs(ID, Name, Data, TransactionDate) VALUES (?,?,?,?)", (recordID, recordName, recordData, recordDate))
        else:
            for record in dataBlobRecords:
                # Name,Data,Checksum,TransactionDate
                recordID = record[0]
                recordName = self.skein.decrypt_string(record[1])
                recordData = self.skein.decrypt_string(record[2])
                recordDate = self.skein.decrypt_string(record[3])
                c.execute("INSERT INTO data_blobs(ID, Name, Data, TransactionDate) VALUES (?,?,?,?)", (recordID, recordName, recordData, recordDate))

    def init_db(self):
        if self.dbms == "sqlite3":
            if os.path.exists(self.config['SQLITE']['db_file']):
                if self.__suppliedOpts.debug:
                    print("VerdandiApp.init_db: Initializing db object as", self.dbms)
                self.db = VerdandiSQLite3(self,self.__suppliedOpts,self.config['SQLITE']['db_file'])
            else:
                # If there isn't a database yet, create a default one to get things rolling
                #self.get_config()
                if os.path.exists(self.config['SQLITE']['db_file']):
                    self.db = VerdandiSQLite3(self,self.__suppliedOpts,self.config['SQLITE']['db_file'])
                else:
                    newDBPath = self.config['SQLITE']['db_file']
                    maskedKeyAndSalt = ("","")
                    self.db = VerdandiSQLite3(self,self.__suppliedOpts,self.config['SQLITE']['db_file'])
                    self.db.create_new_db(newDBPath,maskedKeyAndSalt)
        else:
            if self.__suppliedOpts.debug:
                print("VerdandiApp.init_db: Development required to handle this DBMS:", self.dbms)
            exit("00000001")

    def init_skein(self):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.init_skein.")
        if self.skein == "":
            self.skein = VerdandiSkein(self,self.__suppliedOpts)
    
    def change_key(self, passphrase):
        if self.__suppliedOpts.debug:
            print("VerdandiApp.change_key.")
        self.skein.init_key(passphrase)
        maskedKeyAndSalt = self.skein.get_maskedKey_and_salt()
        print("MaskedKeyAndSalt:", maskedKeyAndSalt)
        if self.db.update_maskedKey_and_salt(maskedKeyAndSalt):
            c = self.memdb.cursor()
            for ID, Name, Data, TransactionDate in c.execute("SELECT * FROM data_blobs"):
                recordID = ID
                newName = self.skein.encrypt_string(Name)
                newData = self.skein.encrypt_string(Data)
                newTransactionDate = self.skein.encrypt_string(TransactionDate)
                self.db.edit_record_commit(recordID,newName,newData,newTransactionDate)
            return True
        else:
            return False

    def get_random_string(self):
        return self.skein.get_random_string()



#EOF
