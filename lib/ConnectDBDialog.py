#!/usrb/bin/env python3
#                                                            05-MAR-2017
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox

# TODO
# verify_path should also check if the file exists and we have read/write permissions

class ConnectDBDialog(tk.Frame):
	"""Defines the GUI"""
	def __init__(self, parent, grandparent, suppliedOpts):
		# parent is this tk instance
		self.parent = parent
		# grandparent is the tk instance that invoked this tk instance (VerdandiGUI)
		self.grandparent = grandparent
		self.__suppliedOpts = suppliedOpts
		self.dbmsSelection = "sqlite3"
		self.dbSelectType = tk.StringVar(master=parent)
		self.dbSelectType.set("sqlite3")
		#
		# Frame
		#
		tk.Frame.__init__(self, parent, borderwidth=1)
		# These help make the window stretchy
		self.masterFrmRow = 0
		self.masterFrmCol = 0
		self.grid_columnconfigure(self.masterFrmRow, weight=1)
		self.grid_rowconfigure(self.masterFrmCol, weight=1)
		self.init_GUI()

	def init_GUI(self):
		#
		# connectDBDialogFrame within parent frame
		#
		# This sits on top of the master frame invoked during instantiation
		self.connectDBDialogFrame = tk.Frame(self, borderwidth=1, relief='raised')
		self.connectDBDialogFrame.grid(row=self.masterFrmRow, column=self.masterFrmCol,	sticky='n'+'s'+'w'+'e')
		connDialogFrmRow = 0
		connDialogFrmCol = 0
		self.connectDBDialogFrame.grid_columnconfigure(connDialogFrmCol, weight=1)
		
		#
		# headingFrame within connectDBDialogFrame
		#
		# headingFrame
		self.headingFrame = tk.Frame(self.connectDBDialogFrame, borderwidth=1, relief='raised')
		self.headingFrame.grid(row=connDialogFrmRow, column=connDialogFrmCol,	sticky='n'+'w'+'e')
		headingFrmRow = 0
		headingFrmCol = 0
		self.headingFrame.grid_columnconfigure(headingFrmCol, weight=1)
		# headingLabel
		self.headingLabel = tk.Label(self.headingFrame, text="Connect To A Database")
		self.headingLabel.grid(row=headingFrmRow, column=headingFrmRow, sticky='n')
		
		#
		# dbSelectFrame within connectDBDialogFrame
		#
		self.dbSelectFrame = tk.Frame(self.connectDBDialogFrame, borderwidth=1, relief='raised')
		connDialogFrmRow += 1
		self.dbSelectFrame.grid(row=connDialogFrmRow, column=connDialogFrmCol,	sticky='n'+'w'+'e')
		dbSelectFrmRow = 0
		dbSelectFrmCol = 0
		# radioSqlite3
		self.radioSqlite3 = tk.Radiobutton(self.dbSelectFrame, text="SQLite3", indicatoron = 0,
														variable=self.dbSelectType, value="sqlite3",
														command=self.toggle_dbms_selection)
		self.radioSqlite3.grid(row=dbSelectFrmRow, column=dbSelectFrmCol, sticky='n'+'w')

		#
		# pathFrame within connectDBDialogFrame
		#
		self.pathFrame = tk.Frame(self.connectDBDialogFrame, borderwidth=1, relief='raised')
		connDialogFrmRow += 1
		self.connectDBDialogFrame.grid_rowconfigure(connDialogFrmRow, weight=1)
		self.pathFrame.grid(row=connDialogFrmRow, column=connDialogFrmCol,	sticky='n'+'w'+'e')
		pathFrmRow = 0
		pathFrmCol = 0
		# openPathLabel
		self.openPathLabel = tk.Label(self.pathFrame, text="Path:")
		self.openPathLabel.grid(row=pathFrmRow, column=pathFrmCol, sticky='w')
		# openPathEntry
		self.openPathEntry = tk.Entry(self.pathFrame, width=0)
		pathFrmCol += 1
		self.pathFrame.grid_columnconfigure(pathFrmCol, weight=1)
		self.openPathEntry.grid(row=pathFrmRow, column=pathFrmCol, sticky='w'+'e')
		# openPathButton
		pathFrmCol += 1
		self.openPathButton = tk.Button(self.pathFrame, text=". . .",
										font=("Helvatica", 3, "bold"),
										command=self.open_file_dialog)
		pathFrmCol += 1
		self.openPathButton.grid(row=pathFrmRow, column=pathFrmCol, sticky='w')
		
		#
		# cancelConnectFrame within connectDBDialogFrame
		#
		self.cancelConnectFrame = tk.Frame(self.connectDBDialogFrame, borderwidth=1, relief='raised')
		connDialogFrmRow += 1
		self.cancelConnectFrame.grid(row=connDialogFrmRow, column=connDialogFrmCol,	sticky='s'+'w'+'e')
		cancelConnFrmRow = 0
		cancelConnFrmCol = 0
		# Cancel Button
		self.cancelButton = tk.Button(self.cancelConnectFrame, text="Cancel",
										font=("Helvatica", 7, "bold"),
										command=lambda: self.master.destroy())
		self.cancelButton.grid(row=cancelConnFrmRow, column=cancelConnFrmCol, sticky='s'+'w')
		#	Create Button
		self.createButton = tk.Button(self.cancelConnectFrame, text="Connect",
										font=("Helvatica", 7, "bold"),
										command=self.connect_database)
		cancelConnFrmCol += 1
		self.cancelConnectFrame.grid_columnconfigure(cancelConnFrmCol, weight=1)
		self.createButton.grid(row=cancelConnFrmRow, column=cancelConnFrmCol, sticky='s'+'e')
		

	# ------------------------------------------------------------------------------------------------------------------
	#  E V E N T   H A N D L E R S
	# ------------------------------------------------------------------------------------------------------------------
	def toggle_dbms_selection(self):
		"""Get the value current value from self.dbSelectType"""
		if self.__suppliedOpts.debug:
			print("ConnectDBDialog.toggle_dbms_selection")
		self.dbmsSelection = self.dbSelectType.get()
		if self.__suppliedOpts.debug:
			print("ConnectDBDialog.toggle_dbms_selection.dbmsSelection:", self.dbmsSelection)


	def verify_path(self):
		"""Check if the filepath is valid"""
		if self.openPathEntry.get() == "":
			if self.__suppliedOpts.debug:
				print("ConnectDBDialog.verify_path: openPathEntry is empty.")
			messagebox.showwarning("Path Warning", "No path was provided.")
			self.parent.lift()
			return False
		else:
			return True

	def open_file_dialog(self):
		"""Call the tkinter askopenfilename dialog. Use it to populate self.openPathEntry"""
		pathstring = filedialog.askopenfilename(defaultextension='.db',
												filetypes=(("SQLite Databases",".db"),
												("All files", "*")))
		self.openPathEntry.delete(0,tk.END)
		self.openPathEntry.insert(0, pathstring)
		self.parent.lift()

	def connect_database(self):
		"""Call the grandparent connect_database method with all our variables"""
		if self.__suppliedOpts.debug:
			print("ConnectDBDialog.connect_database: grandparent.app.create_new_database()")
			print("ConnectDBDialog.connect_database: dbms is", self.dbmsSelection)
			print("ConnectDBDialog.connect_database: dbName is", self.openPathEntry.get())
		if self.verify_path():
			# grandma will close this tk instance when she is ready
			self.grandparent.connect_database(self.dbmsSelection,self.openPathEntry.get())
			
			
			
			


#EOF
