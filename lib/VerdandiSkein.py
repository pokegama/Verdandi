#!/usr/bin/env python3
#                                                            25-FEB-2017
#  VerdandiSkein.py
#
#  The Verdandi cryptographic services

from skein import skein512, threefish
from random import randrange
from os import urandom
from hashlib import pbkdf2_hmac
from binascii import hexlify,unhexlify

class VerdandiSkein:
	def __init__(self, parent, suppliedOpts):
		self.__suppliedOpts = suppliedOpts
		self.parent = parent
		self.cipherBlockSize = 64
		self.salt = ""
		self.key = ""
		self.maskedKey = ""

	def init_key(self,passphrase):
		if self.__suppliedOpts.debug:
			print("VerdandiSkein.init_key.")
		passphraseBytes = passphrase.encode("utf-8")
		self.key = skein512(passphraseBytes, digest_bits=self.cipherBlockSize*8).digest()
		saltBytes = urandom(64)
		self.salt = hexlify(saltBytes).decode("utf-8")
		skeinMask = skein512(self.key, digest_bits=self.cipherBlockSize*8).digest()
		self.maskedKey = hexlify(pbkdf2_hmac('sha512', skeinMask, saltBytes, 100000)).decode("utf-8")
		
	def check_passphrase(self, passphrase, maskedKey, salt):
		if self.__suppliedOpts.debug:
			print("VerdandiSkein.generateMask.")
		passphraseBytes = passphrase.encode("utf-8")
		saltBytes = unhexlify(salt.encode("utf-8"))
		testKey = skein512(passphraseBytes, digest_bits=self.cipherBlockSize*8).digest()
		testSkeinMask = skein512(testKey, digest_bits=self.cipherBlockSize*8).digest()
		testMaskedKey = hexlify(pbkdf2_hmac('sha512', testSkeinMask, saltBytes, 100000)).decode("utf-8")
		if testMaskedKey == maskedKey:
			self.key = testKey
			self.salt = salt
			self.maskedKey = testMaskedKey
			return True
		else:
			return False
	
	def get_maskedKey_and_salt(self):
		if self.__suppliedOpts.debug:
			print("VerdandiSkein.get_keysalt.")
		return (self.maskedKey,self.salt)
		
	def get_key(self):
		if self.__suppliedOpts.debug:
			print("VerdandiSkein.get_key.")
		return self.key
		
	def get_maskedKey(self):
		if self.__suppliedOpts.debug:
			print("VerdandiSkein.get_maskedKey.")
		return self.maskedKey
		
	def get_salt(self):
		if self.__suppliedOpts.debug:
			print("VerdandiSkein.get_salt.")
		return self.salt
		
	def hash_byte_obj(self,obj):
		if self.__suppliedOpts.debug:
			print("VerdandiSkein.hash_byte_obj: ", obj)
		hasher = skein512()
		hasher.update(obj)
		return hasher.digest()
		
	def hash_string_obj(self,string):
		if self.__suppliedOpts.debug:
			print("VerdandiSkein.hash_string_obj: ", string)
		byteString = string.encode("utf-8")
		hasher = skein512()
		hasher.update(byteString)
		return hasher.digest()

	def encrypt_string(self,string):
		cipherBlockSize = self.cipherBlockSize
		byteString = string.encode("utf-8")
		# generate random tweak value and write it at the beginning of the file
		tweak = bytes(randrange(256) for _ in range(16))
		encryptedByteString = tweak
		cipher = threefish(self.key, tweak)
		lenString = len(byteString)
		if lenString <= cipherBlockSize:
			blocks = 1
		elif lenString % cipherBlockSize != 0:
			blocks = (lenString // cipherBlockSize) + 1
		else:
			blocks = lenString // cipherBlockSize
		if lenString < (blocks * cipherBlockSize):
			paddingNeeded = (blocks * cipherBlockSize) - lenString
			paddedString = byteString + (b'\x00' * paddingNeeded)
		else:
			paddedString = byteString
		tweakLen = 16
		start = 0
		stop = cipherBlockSize
		for block in range(blocks):
			encryptedByteBlock = cipher.encrypt_block(paddedString[start:stop])
			start += cipherBlockSize
			stop += cipherBlockSize
			cipher.tweak = encryptedByteBlock[:tweakLen]
			encryptedByteString += encryptedByteBlock
		return hexlify(encryptedByteString).decode("utf-8") 
		
	def decrypt_string(self,cipherText):
		cipherBlockSize = self.cipherBlockSize
		encryptedByteString = unhexlify(cipherText.encode("utf-8"))
		tweak = encryptedByteString[:16]
		cipher = threefish(self.key,tweak)
		blocks = len(encryptedByteString) // cipherBlockSize
		byteString = b''
		tweakLen = 16
		start = tweakLen
		stop = start + cipherBlockSize
		for block in range(blocks):
			encryptedByteBlock = encryptedByteString[start:stop]
			byteString += cipher.decrypt_block(encryptedByteBlock)
			start += cipherBlockSize
			stop += cipherBlockSize
			cipher.tweak = encryptedByteBlock[:tweakLen]
		byteString = byteString.rstrip(b'\00')
		plainText = byteString.decode("utf-8")
		return plainText
		
	def get_random_string(self):
		randomCharList = []
		for i in range(16):
			randomCharList.append(chr(randrange(33,126)))
		return "".join(randomCharList)
