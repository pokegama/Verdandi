# Verdandi
Verdandi is a simple, but very secure desktop application that was originally
envisioned as a password management tool, but it can serve as a secure storage
container for just about any text content.

It stores data as records in an SQLite database.  Because it can be used as a
generic text storage tool, encryption is not a mandatory option.  If a person
**does** wish to secure their content, the records are encrypted with
[Threefish-512](https://www.schneier.com/academic/skein/threefish.html), and
no cleartext content will ever be written to disk.  The records are stored in
memory, and encrypted before being stored in the database.  The password is
processed through the Skein hash function to create the 512-bit key.  Before
being stored, the key is processed through 100,000 iterations of PBKDF2 with a
random 512-bit salt.


## Installation
Verdandi uses the [Skein](https://pythonhosted.org/pyskein/) module for Python.

### Debian 9
1. You'll need PIP and TKinter for Python.
~~~~
sudo apt-get install python3-pip python3-tk
~~~~

2. Install the Skein module.
~~~~
pip3 install pyskein --user
~~~~
or
~~~~
sudo pip3 install pyskein
~~~~


### Fedora
1. On Fedora, python3-devel and redhat-rpm-config are needed to build the Skein
module.  You'll also want to verify you have TKinter installed.
~~~~
sudo dnf install python3-devel python3-tkinter redhat-rpm-config
~~~~

2. A C compiler and libraries are needed to build Skein also.
~~~~
sudo dnf group install "Development Tools" "C Development Tools and Libraries"
~~~~

3. Then attempt to install Skein
~~~~
pip3 install pyskein --user
~~~~
or
~~~~
sudo pip3 install pyskein
~~~~


### Windows
1. On Windows, install visualcppbuildtools_full.exe

2. Then attempt to install Skein
~~~~
pip install pyskein
~~~~